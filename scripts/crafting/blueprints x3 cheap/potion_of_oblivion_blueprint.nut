this.potion_of_oblivion_blueprint <- this.inherit("scripts/crafting/blueprint", {
	m = {},
	function create()
	{
		this.blueprint.create();
		this.m.ID = "blueprint.potion_of_oblivion";
		this.m.PreviewCraftable = this.new("scripts/items/misc/potion_of_oblivion_item");
		this.m.Cost = 1;
		local ingredients = [
			{
				Script = "scripts/items/supplies/ground_grains_item",
				Num = 1
			}
		];
		this.init(ingredients);
	}

	function onCraft( _stash )
	{
		_stash.add(this.new("scripts/items/misc/potion_of_oblivion_item"));
		_stash.add(this.new("scripts/items/misc/potion_of_oblivion_item"));
		_stash.add(this.new("scripts/items/misc/potion_of_oblivion_item"));
	}

});

